SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WORK_LOG](
	[LOG_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LOG_DT] [datetime] NOT NULL,
	[STATE] [varchar](20) NOT NULL,
	[FROM_DATA] [varchar](10) NOT NULL,
	[TO_DATA] [varchar](10) NOT NULL,
	[REF_DOC] [varchar](10) NOT NULL,
	[REF_DOCNO] [varchar](70) NOT NULL,
	[ERROR_MSG] [text] NULL,
 CONSTRAINT [PK_WORK_LOG] PRIMARY KEY NONCLUSTERED 
(
	[LOG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [CIX_WORK_LOG_LOG_DT]    Script Date: 2021-11-10 오후 9:36:53 ******/
CREATE CLUSTERED INDEX [CIX_WORK_LOG_LOG_DT] ON [dbo].[WORK_LOG]
(
	[LOG_DT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_WORK_LOG_REF_DOCNO]    Script Date: 2021-11-10 오후 9:36:53 ******/
CREATE NONCLUSTERED INDEX [IX_WORK_LOG_REF_DOCNO] ON [dbo].[WORK_LOG]
(
	[REF_DOCNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WORK_LOG] ADD  CONSTRAINT [DF_WORK_LOG_LOG_DT]  DEFAULT (getdate()) FOR [LOG_DT]
GO
ALTER TABLE [dbo].[WORK_LOG] ADD  CONSTRAINT [DF_WORK_LOG_STATE]  DEFAULT ('SUCCESS') FOR [STATE]
GO
ALTER TABLE [dbo].[WORK_LOG] ADD  CONSTRAINT [DF_WORK_LOG_FROM_DATA]  DEFAULT ('ERP') FOR [FROM_DATA]
GO
ALTER TABLE [dbo].[WORK_LOG] ADD  CONSTRAINT [DF_WORK_LOG_TO_DATA]  DEFAULT ('EDI') FOR [TO_DATA]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'로그번호' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG', @level2type=N'COLUMN',@level2name=N'LOG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'로그기록일시' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG', @level2type=N'COLUMN',@level2name=N'LOG_DT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SUCCESS : 성공
CONFLICT : 중복
FAILED : 실패' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG', @level2type=N'COLUMN',@level2name=N'STATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'문서코드' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG', @level2type=N'COLUMN',@level2name=N'REF_DOC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'문서번호' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG', @level2type=N'COLUMN',@level2name=N'REF_DOCNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'에러메세지' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG', @level2type=N'COLUMN',@level2name=N'ERROR_MSG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'업로드 로그 테이블' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WORK_LOG'
GO
