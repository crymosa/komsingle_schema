create table IF_KIS_ERP_007
(
	ERP_ID VARCHAR2(35) not null
		constraint PK_IF_KIS_ERP_007
			primary key,
	APP_DATE VARCHAR2(8) not null,
	EXE_DATE VARCHAR2(8),
	BF_CODE VARCHAR2(3),
	ADREFNO VARCHAR2(35),
	BUS_CD VARCHAR2(3),
	BUS_CD1 VARCHAR2(3),
	CHARGE_TO VARCHAR2(3),
	PAY_AMT NUMBER(16,3),
	PAY_AMTC VARCHAR2(3),
	PAY_AMT2 NUMBER(16,3),
	PAY_AMT2C VARCHAR2(3),
	OD_BANK1 VARCHAR2(54),
	OD_NAME1 VARCHAR2(54),
	OD_ACCNT1 VARCHAR2(35),
	OD_ACCNT2 VARCHAR2(54),
	OD_CURR VARCHAR2(3),
	OD_NATION VARCHAR2(3),
	EC_BANK VARCHAR2(11),
	EC_BANK1 VARCHAR2(54),
	EC_NAME1 VARCHAR2(54),
	EC_ACCNT1 VARCHAR2(17),
	RTN_CODE       VARCHAR2(4)
	RTN_MSG        VARCHAR2(2000)
	RTN_DTM         DATE
)
/

comment on table IF_KIS_ERP_007 is '지급지시서'
/

comment on column IF_KIS_ERP_007.ERP_ID is '관리번호'
/

comment on column IF_KIS_ERP_007.APP_DATE is '신청일자'
/

comment on column IF_KIS_ERP_007.EXE_DATE is '이체희망일자'
/

comment on column IF_KIS_ERP_007.BF_CODE is '거래형태구분'
/

comment on column IF_KIS_ERP_007.ADREFNO is '기타참조번호'
/

comment on column IF_KIS_ERP_007.BUS_CD is '지급지시서 용도'
/

comment on column IF_KIS_ERP_007.BUS_CD1 is '납부수수료 유형'
/

comment on column IF_KIS_ERP_007.CHARGE_TO is '부가수수료부담자'
/

comment on column IF_KIS_ERP_007.PAY_AMT is '지급총액'
/

comment on column IF_KIS_ERP_007.PAY_AMTC is '지급총액 통화단위'
/

comment on column IF_KIS_ERP_007.PAY_AMT2 is '외화계좌 지급금액'
/

comment on column IF_KIS_ERP_007.PAY_AMT2C is '지급금액 통화단위'
/

comment on column IF_KIS_ERP_007.OD_BANK1 is '외화계좌 은행명'
/

comment on column IF_KIS_ERP_007.OD_NAME1 is '외화계좌 지점명'
/

comment on column IF_KIS_ERP_007.OD_ACCNT1 is '외화계좌 계좌번호'
/

comment on column IF_KIS_ERP_007.OD_ACCNT2 is '외화계좌 계좌주'
/

comment on column IF_KIS_ERP_007.OD_CURR is '외화계좌 통화단위'
/

comment on column IF_KIS_ERP_007.OD_NATION is '외화게좌 국가코드'
/

comment on column IF_KIS_ERP_007.EC_BANK is '원화계좌 은행코드'
/

comment on column IF_KIS_ERP_007.EC_BANK1 is '원화계좌 은행명'
/

comment on column IF_KIS_ERP_007.EC_NAME1 is '원화계좌 지점명'
/

comment on column IF_KIS_ERP_007.EC_ACCNT1 is '원화계좌 계좌번호'
/

comment on column IF_KIS_ERP_007.RTN_CODE is '상태 코드'
/

comment on column IF_KIS_ERP_007.RTN_MSG is '상태 메시지'
/

comment on column IF_KIS_ERP_007.RTN_DTM is '수정 일시'
/

