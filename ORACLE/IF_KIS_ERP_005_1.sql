create table IF_KIS_ERP_005_1
(
	EDI_ID VARCHAR2(35) not null
		constraint FK_IF_KIS_ERP_005_1_EDI_ID_IF_
			references IF_KIS_ERP_005,
	SEQ NUMBER not null,
	CHCODE VARCHAR2(3),
	BILLNO VARCHAR2(35),
	CHRATE NUMBER(18,4),
	AMT1 NUMBER(18,4),
	AMT1C VARCHAR2(3),
	AMT2 NUMBER(18,4),
	AMT2C VARCHAR2(3),
	KRWRATE NUMBER(18,4),
	CHDAY NUMBER,
	CHDATE1 VARCHAR2(8),
	CHDATE2 VARCHAR2(8), 
	CONSTRAINT PK_IF_KIS_ERP_005_1 PRIMARY KEY 
  (
    EDI_ID 
  , SEQ 
  )
)
/

comment on table IF_KIS_ERP_005_1 is '계산서 세부내용'
/

comment on column IF_KIS_ERP_005_1.EDI_ID is 'EDI 문서 신청번호'
/

comment on column IF_KIS_ERP_005_1.SEQ is '순번'
/

comment on column IF_KIS_ERP_005_1.CHCODE is '수수료_이자유형'
/

comment on column IF_KIS_ERP_005_1.BILLNO is '계산서번호'
/

comment on column IF_KIS_ERP_005_1.CHRATE is '적용요율'
/

comment on column IF_KIS_ERP_005_1.AMT1 is '대상금액'
/

comment on column IF_KIS_ERP_005_1.AMT1C is '통화단위'
/

comment on column IF_KIS_ERP_005_1.AMT2 is '산출금액'
/

comment on column IF_KIS_ERP_005_1.AMT2C is '통화단위'
/

comment on column IF_KIS_ERP_005_1.KRWRATE is '환율'
/

comment on column IF_KIS_ERP_005_1.CHDAY is '적용일수'
/

comment on column IF_KIS_ERP_005_1.CHDATE1 is '적용기간시작'
/

comment on column IF_KIS_ERP_005_1.CHDATE2 is '적용기간종료'
/


