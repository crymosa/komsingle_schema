create table IF_KIS_ERP_005
(
	EDI_ID VARCHAR2(35) not null
		constraint PK_IF_KIS_ERP_005
			primary key,
	DATEE VARCHAR2(8) not null,
	BUS VARCHAR2(3),
	BUS_DESC VARCHAR2(105),
	DOC_NO1 VARCHAR2(35),
	DOC_NO2 VARCHAR2(35),
	DOC_NO3 VARCHAR2(35),
	DOC_NO4 VARCHAR2(35),
	TRN_DATE VARCHAR2(8),
	ADV_DATE VARCHAR2(8),
	REMARK1 VARCHAR2(525),
	BANK VARCHAR2(11),
	BANK_NAME1 VARCHAR2(105),
	BANK_NAME2 VARCHAR2(105),
	CUST_NAME1 VARCHAR2(54),
	CUST_NAME2 VARCHAR2(54),
	CUST_NAME3 VARCHAR2(54),
	PAYORD1 NUMBER(18,4),
	PAYORD11 NUMBER(18,4),
	PAYORD1C VARCHAR2(3),
	RATE1 NUMBER(18,4),
	PAYORD2 NUMBER(18,4),
	PAYORD21 NUMBER(18,4),
	PAYORD2C VARCHAR2(3),
	RATE2 NUMBER(18,4),
	PAYORD3 NUMBER(18,4),
	PAYORD31 NUMBER(18,4),
	PAYORD3C VARCHAR2(3),
	RATE3 NUMBER(18,4),
	PAYORD4 NUMBER(18,4),
	PAYORD41 NUMBER(18,4),
	PAYORD4C VARCHAR2(3),
	RATE4 NUMBER(18,4),
	PAYORD5 NUMBER(18,4),
	PAYORD51 NUMBER(18,4),
	PAYORD5C VARCHAR2(3),
	RATE5 NUMBER(18,4),
	FOBRATE NUMBER(18,4),
	RATEIN NUMBER(18,4),
	FINAMT1 NUMBER(18,4),
	FINAMT1C VARCHAR2(3),
	FINAMT2 NUMBER(18,4),
	FINAMT2C VARCHAR2(3),
	ERP_ID VARCHAR2(35)
)
/

comment on table IF_KIS_ERP_005 is '계산서'
/

comment on column IF_KIS_ERP_005.EDI_ID is 'EDI 문서 신청번호'
/

comment on column IF_KIS_ERP_005.DATEE is '수신일자'
/

comment on column IF_KIS_ERP_005.BUS is '계산서용도'
/

comment on column IF_KIS_ERP_005.BUS_DESC is '계산서 세부거래명'
/

comment on column IF_KIS_ERP_005.DOC_NO1 is '신용장_계약서번호'
/

comment on column IF_KIS_ERP_005.DOC_NO2 is '신청번호'
/

comment on column IF_KIS_ERP_005.DOC_NO3 is '참조번호'
/

comment on column IF_KIS_ERP_005.DOC_NO4 is '기타번호'
/

comment on column IF_KIS_ERP_005.TRN_DATE is '거래일자'
/

comment on column IF_KIS_ERP_005.ADV_DATE is '통지일자'
/

comment on column IF_KIS_ERP_005.REMARK1 is '기타정보'
/

comment on column IF_KIS_ERP_005.BANK is '발급은행코드'
/

comment on column IF_KIS_ERP_005.BANK_NAME1 is '발급은행명'
/

comment on column IF_KIS_ERP_005.BANK_NAME2 is '발급은행지점'
/

comment on column IF_KIS_ERP_005.CUST_NAME1 is '거래고객명'
/

comment on column IF_KIS_ERP_005.CUST_NAME2 is '거래고객정보1'
/

comment on column IF_KIS_ERP_005.CUST_NAME3 is '거래고객정보2'
/

comment on column IF_KIS_ERP_005.PAYORD1 is '외화총액'
/

comment on column IF_KIS_ERP_005.PAYORD11 is '원화환산금액'
/

comment on column IF_KIS_ERP_005.PAYORD1C is '외화계좌 송금액 통화단위'
/

comment on column IF_KIS_ERP_005.RATE1 is '적용환율'
/

comment on column IF_KIS_ERP_005.PAYORD2 is '외화대체'
/

comment on column IF_KIS_ERP_005.PAYORD3 is '현물환'
/

comment on column IF_KIS_ERP_005.PAYORD4 is '선물환'
/

comment on column IF_KIS_ERP_005.PAYORD5 is '수입보증금'
/

comment on column IF_KIS_ERP_005.FOBRATE is 'FOB 환산율'
/

comment on column IF_KIS_ERP_005.RATEIN is '보증금적립율'
/

comment on column IF_KIS_ERP_005.FINAMT1 is '최종지급_수입액'
/

comment on column IF_KIS_ERP_005.FINAMT1C is '통화단위'
/

comment on column IF_KIS_ERP_005.FINAMT2 is '수수료_이자합계'
/

comment on column IF_KIS_ERP_005.FINAMT2C is '통화단위'
/

comment on column IF_KIS_ERP_005.ERP_ID is 'ERP에서 전송한 키값'
/

