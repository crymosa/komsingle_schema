create table IF_KIS_ERP_003
(
	ERP_ID VARCHAR2(35) not null
		constraint PK_IF_KIS_ERP_003
			primary key,
	APP_DT VARCHAR2(10) not null,
	REG_DT DATE not null,
	ETC_NO VARCHAR2(70),
	TRD_CD VARCHAR2(3) not null,
	IMPT_REMIT_CD VARCHAR2(3),
	REMIT_CD VARCHAR2(1),
	ADDED_FEE_TAR VARCHAR2(3),
	TOTAL_AMT NUMBER(19,4),
	TOTAL_AMT_UNIT VARCHAR2(3),
	FORE_AMT NUMBER(19,4),
	FORE_AMT_UNIT VARCHAR2(3),
	BF_ACCNT_NO VARCHAR2(50),
	BF_ACCNT_NM VARCHAR2(35),
	BF_ACCNT_UNIT VARCHAR2(3),
	BF_BNK_CD VARCHAR2(11),
	BF_BNK_NM1 VARCHAR2(70),
	BF_BNK_NM2 VARCHAR2(70),
	BF_BNK_NAT_CD VARCHAR2(3),
	EC_BNK_CD VARCHAR2(11),
	OR_BNK_CD VARCHAR2(11),
	BE_BUS_NM VARCHAR2(105),
	BE_BUS_ADDR1 VARCHAR2(35),
	BE_BUS_ADDR2 VARCHAR2(35),
	BE_BUS_ADDR3 VARCHAR2(35),
	BE_NAT_CD VARCHAR2(3),
	RFF_NO VARCHAR2(35),
	AMT NUMBER(18,4),
	AMT_UNIT VARCHAR2(3),
	CIF_AMT NUMBER(19,4),
	CIF_AMT_UNIT VARCHAR2(3),
	IMPT_CD VARCHAR2(3),
	CONT_DT VARCHAR2(10),
	TOD_CD VARCHAR2(3),
	TOD_REMARK VARCHAR2(70),
	EXPORT_NAT_CD VARCHAR2(2),
	EXPORT_OW_NM VARCHAR2(105),
	DETAIL_DESC1 VARCHAR2(70),
	DETAIL_DESC2 VARCHAR2(70),
	DETAIL_DESC3 VARCHAR2(70),
	DETAIL_DESC4 VARCHAR2(70),
	DETAIL_DESC5 VARCHAR2(70),
	GOODS_NM VARCHAR2(54),
	RTN_CODE       VARCHAR2(4)
	RTN_MSG        VARCHAR2(2000)
	RTN_DTM         DATE
)
/

comment on table IF_KIS_ERP_003 is '수입송금 신청서'
/

comment on column IF_KIS_ERP_003.ERP_ID is 'ERP 키값'
/

comment on column IF_KIS_ERP_003.APP_DT is '신청일자'
/

comment on column IF_KIS_ERP_003.REG_DT is '등록일자'
/

comment on column IF_KIS_ERP_003.ETC_NO is '기타번호'
/

comment on column IF_KIS_ERP_003.TRD_CD is '거래형태구분'
/

comment on column IF_KIS_ERP_003.IMPT_REMIT_CD is '수입송금신청 용도코드'
/

comment on column IF_KIS_ERP_003.REMIT_CD is '송금방법'
/

comment on column IF_KIS_ERP_003.ADDED_FEE_TAR is '부가수수료부담자'
/

comment on column IF_KIS_ERP_003.TOTAL_AMT is '총 송금액'
/

comment on column IF_KIS_ERP_003.TOTAL_AMT_UNIT is '총 송금액 통화단위'
/

comment on column IF_KIS_ERP_003.FORE_AMT is '외화계좌 송금액'
/

comment on column IF_KIS_ERP_003.FORE_AMT_UNIT is '외화계좌 송금액 통화단위'
/

comment on column IF_KIS_ERP_003.BF_ACCNT_NO is '수익자은행 계좌번호'
/

comment on column IF_KIS_ERP_003.BF_ACCNT_NM is '수익자은행 계좌주'
/

comment on column IF_KIS_ERP_003.BF_ACCNT_UNIT is '수익자은행 통화코드'
/

comment on column IF_KIS_ERP_003.EC_BNK_CD is '송금의뢰은행코드 (원화원금계좌)'
/

comment on column IF_KIS_ERP_003.OR_BNK_CD is '송금의뢰은행코드 (외화원금계좌)'
/

comment on column IF_KIS_ERP_003.BF_BNK_CD is '수익자은행코드'
/

comment on column IF_KIS_ERP_003.BF_BNK_NM1 is '수익자은행명'
/

comment on column IF_KIS_ERP_003.BF_BNK_NM2 is '수익자은행 지점명'
/

comment on column IF_KIS_ERP_003.BF_BNK_NAT_CD is '수익자은행 국가코드'
/

comment on column IF_KIS_ERP_003.BE_BUS_NM is '수익자 상호명'
/

comment on column IF_KIS_ERP_003.BE_BUS_ADDR1 is '주소1'
/

comment on column IF_KIS_ERP_003.BE_BUS_ADDR2 is '주소2'
/

comment on column IF_KIS_ERP_003.BE_BUS_ADDR3 is '주소3'
/

comment on column IF_KIS_ERP_003.BE_NAT_CD is '의뢰은행 국가코드'
/

comment on column IF_KIS_ERP_003.RFF_NO is '계약서번호_수입신고필증번호'
/

comment on column IF_KIS_ERP_003.AMT is '결제금액_계약금액'
/

comment on column IF_KIS_ERP_003.AMT_UNIT is '통화코드'
/

comment on column IF_KIS_ERP_003.CIF_AMT is '과세가격'
/

comment on column IF_KIS_ERP_003.CIF_AMT_UNIT is '통화코드'
/

comment on column IF_KIS_ERP_003.IMPT_CD is '수입용도'
/

comment on column IF_KIS_ERP_003.CONT_DT is '계약일자'
/

comment on column IF_KIS_ERP_003.TOD_CD is '가격조건'
/

comment on column IF_KIS_ERP_003.TOD_REMARK is '가격조건 수기입력'
/

comment on column IF_KIS_ERP_003.EXPORT_NAT_CD is '적출국_수출국'
/

comment on column IF_KIS_ERP_003.EXPORT_OW_NM is '수출자상호'
/

comment on column IF_KIS_ERP_003.DETAIL_DESC1 is '상세정보1'
/

comment on column IF_KIS_ERP_003.GOODS_NM is '대표물품명'
/

comment on column IF_KIS_ERP_003.RTN_CODE is '상태 코드'
/

comment on column IF_KIS_ERP_003.RTN_MSG is '상태 메시지'
/

comment on column IF_KIS_ERP_003.RTN_DTM is '수정 일시'
/