create table IF_KIS_ERP_001
(
	ERP_ID VARCHAR2(35) not null
		constraint PK_IF_KIS_ERP_001
			primary key,
	APP_DATE VARCHAR2(8) not null,
	AD_BANK1 VARCHAR2(70) not null,
	AD_BANK2 VARCHAR2(70),
	AD_BANK_BIC VARCHAR2(11),
	AD_PAY VARCHAR2(3),
	EX_DATE VARCHAR2(8) not null,
	APPLIC_BRAND VARCHAR2(35),
	BENEFC1 VARCHAR2(70) not null,
	BENEFC3 VARCHAR2(70),
	BENEFC5 VARCHAR2(35),
	CD_AMT NUMBER(18,4) not null,
	CD_CUR VARCHAR2(3) not null,
	CD_PERP NUMBER(18,4),
	CD_PERM NUMBER(18,4),
	DRAFT VARCHAR2(105),
	DEF_PAY VARCHAR2(140),
	PSHIP VARCHAR2(3),
	TSHIP VARCHAR2(3),
	LOAD_ON VARCHAR2(65),
	FOR_TRAN VARCHAR2(65),
	LST_DATE VARCHAR2(8),
	DESGOOD_1 CLOB,
	DOC_380 NUMBER(3),
	DOC_380_1 NUMBER(18,4),
	DOC_705 NUMBER(3),
	DOC_705_GUBUN VARCHAR2(3),
	DOC_705_1 VARCHAR2(35),
	DOC_705_3 VARCHAR2(3),
	DOC_705_4 VARCHAR2(35),
	DOC_740 NUMBER(3),
	DOC_740_1 VARCHAR2(35),
	DOC_740_3 VARCHAR2(3),
	DOC_740_4 VARCHAR2(35),
	DOC_530 NUMBER(3),
	DOC_530_1 VARCHAR2(65),
	DOC_271 NUMBER(3),
	DOC_271_1 NUMBER(18,4),
	DOC_861 NUMBER(3),
	DOC_2AA NUMBER(3),
	DOC_2AA_1 CLOB,
	ACD_2AA NUMBER(3),
	ACD_2AA_1 VARCHAR2(35),
	REI_TXT CLOB,
	FTA_TXT CLOB,
	ACD_2AE NUMBER(3),
	ACD_2AE_1 CLOB,
	CHARGE VARCHAR2(3),
	CHARGE_1 CLOB,
	PERIOD NUMBER(18,4),
	PERIOD_IDX NUMBER,
	PERIOD_TXT VARCHAR2(35),
	CONFIRMM VARCHAR2(3),
	CONFIRM_BICCD VARCHAR2(11),
	CONFIRM_BANKNM VARCHAR2(70),
	CONFIRM_BANKBR VARCHAR2(70),
	ORIGIN VARCHAR2(3),
	ORIGIN_M VARCHAR2(65),
	PL_TERM VARCHAR2(65),
	TERM_PR VARCHAR2(3),
	CARRIAGE VARCHAR2(3),
	SUNJUCK_PORT VARCHAR2(65),
	DOCHACK_PORT VARCHAR2(65),
	RTN_CODE       VARCHAR2(4)
	RTN_MSG        VARCHAR2(2000)
	RTN_DTM         DATE
)
/

comment on table IF_KIS_ERP_001 is '수입신용장 신청서'
/

comment on column IF_KIS_ERP_001.ERP_ID is 'ERP 키값'
/

comment on column IF_KIS_ERP_001.APP_DATE is '개설신청일'
/

comment on column IF_KIS_ERP_001.AD_BANK1 is '통지은행명'
/

comment on column IF_KIS_ERP_001.AD_BANK2 is '통지은행 지점명'
/

comment on column IF_KIS_ERP_001.AD_BANK_BIC is '통지은행 BIC코드'
/

comment on column IF_KIS_ERP_001.AD_PAY is '신용공여코드'
/

comment on column IF_KIS_ERP_001.EX_DATE is '신용장 기한일'
/

comment on column IF_KIS_ERP_001.APPLIC_BRAND is '수입자 브랜드명'
/

comment on column IF_KIS_ERP_001.BENEFC1 is '수익자 상호'
/

comment on column IF_KIS_ERP_001.BENEFC3 is '수익자 주소'
/

comment on column IF_KIS_ERP_001.BENEFC5 is '수익자 계좌번호'
/

comment on column IF_KIS_ERP_001.CD_AMT is '계약금액'
/

comment on column IF_KIS_ERP_001.CD_CUR is '통화단위'
/

comment on column IF_KIS_ERP_001.CD_PERP is '초과허용율'
/

comment on column IF_KIS_ERP_001.CD_PERM is '부족허용율'
/

comment on column IF_KIS_ERP_001.DRAFT is '화환어음조건'
/

comment on column IF_KIS_ERP_001.DEF_PAY is '매입_연지급조건명세'
/

comment on column IF_KIS_ERP_001.PSHIP is '분할선적 허용여부'
/

comment on column IF_KIS_ERP_001.TSHIP is '환적 허용여부'
/

comment on column IF_KIS_ERP_001.LOAD_ON is '수탁지_발송지'
/

comment on column IF_KIS_ERP_001.FOR_TRAN is '최종 목적지'
/

comment on column IF_KIS_ERP_001.LST_DATE is '최종 선적일자'
/

comment on column IF_KIS_ERP_001.DESGOOD_1 is '상품_용역명세'
/

comment on column IF_KIS_ERP_001.DOC_380 is 'Commercial Invoice Check'
/

comment on column IF_KIS_ERP_001.DOC_380_1 is 'Commercial Invoice Copy Number'
/

comment on column IF_KIS_ERP_001.CHARGE is '수수료부담자'
/

comment on column IF_KIS_ERP_001.CHARGE_1 is '수수료부담자 직접입력'
/

comment on column IF_KIS_ERP_001.PERIOD is '서류제시기간'
/

comment on column IF_KIS_ERP_001.PERIOD_TXT is '서류제시기간 직접입력'
/

comment on column IF_KIS_ERP_001.CONFIRMM is '확인지시문언'
/

comment on column IF_KIS_ERP_001.CONFIRM_BICCD is '확인은행 BIC코드'
/

comment on column IF_KIS_ERP_001.CONFIRM_BANKNM is '확인은행명'
/

comment on column IF_KIS_ERP_001.CONFIRM_BANKBR is '확인은행 지점명'
/

comment on column IF_KIS_ERP_001.ORIGIN is '원산지 국가'
/

comment on column IF_KIS_ERP_001.ORIGIN_M is '국가명'
/

comment on column IF_KIS_ERP_001.PL_TERM is '가격조건 관련장소'
/

comment on column IF_KIS_ERP_001.TERM_PR is '정형거래조건/가격조건'
/

comment on column IF_KIS_ERP_001.CARRIAGE is '운송수단'
/

comment on column IF_KIS_ERP_001.SUNJUCK_PORT is '선적항'
/

comment on column IF_KIS_ERP_001.DOCHACK_PORT is '도착항'
/

comment on column IF_KIS_ERP_001.RTN_CODE is '상태 코드'
/

comment on column IF_KIS_ERP_001.RTN_MSG is '상태 메시지'
/

comment on column IF_KIS_ERP_001.RTN_DTM is '수정 일시'
/

