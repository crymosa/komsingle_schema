create table IF_KIS_ERP_006
(
	EDI_ID VARCHAR2(35) not null
		constraint PK_IF_KIS_ERP_006
			primary key,
	DATEE VARCHAR2(8) not null,
	APP_NAME1 VARCHAR2(54),
	APP_NAME2 VARCHAR2(54),
	APP_NAME3 VARCHAR2(54),
	BANK1 VARCHAR2(105),
	BANK2 VARCHAR2(105),
	LC_G VARCHAR2(3),
	LC_NO VARCHAR2(35),
	BL_G VARCHAR2(3),
	BL_NO VARCHAR2(35),
	AMT NUMBER(18,4),
	AMTC VARCHAR2(3),
	CHRG NUMBER(18,4),
	CHRGC VARCHAR2(3),
	RES_DATE VARCHAR2(8),
	SET_DATE VARCHAR2(8),
	REMARK1 CLOB,
	BK_NAME1 VARCHAR2(54),
	BK_NAME2 VARCHAR2(54),
	BK_NAME3 VARCHAR2(54)
)
/

comment on table IF_KIS_ERP_006 is '선적서류 도착 통보서'
/

comment on column IF_KIS_ERP_006.EDI_ID is '관리번호'
/

comment on column IF_KIS_ERP_006.DATEE is '수신일자'
/

comment on column IF_KIS_ERP_006.APP_NAME1 is '수신업체명1'
/

comment on column IF_KIS_ERP_006.APP_NAME2 is '수신업체명2'
/

comment on column IF_KIS_ERP_006.APP_NAME3 is '수신업체명3'
/

comment on column IF_KIS_ERP_006.BANK1 is '통지은행명1'
/

comment on column IF_KIS_ERP_006.BANK2 is '통지은행명2'
/

comment on column IF_KIS_ERP_006.LC_G is '신용장 분류코드'
/

comment on column IF_KIS_ERP_006.LC_NO is '신용장 번호'
/

comment on column IF_KIS_ERP_006.BL_G is '선하증권 분류코드'
/

comment on column IF_KIS_ERP_006.BL_NO is '선하증권 번호'
/

comment on column IF_KIS_ERP_006.AMT is '어음금액'
/

comment on column IF_KIS_ERP_006.AMTC is '어음금액 통화단위'
/

comment on column IF_KIS_ERP_006.CHRG is '기타수수료'
/

comment on column IF_KIS_ERP_006.CHRGC is '기타수수료 통화단위'
/

comment on column IF_KIS_ERP_006.RES_DATE is '통지일자'
/

comment on column IF_KIS_ERP_006.SET_DATE is '최종결제일'
/

comment on column IF_KIS_ERP_006.REMARK1 is '기타사항'
/

comment on column IF_KIS_ERP_006.BK_NAME1 is '발신기관명1'
/

comment on column IF_KIS_ERP_006.BK_NAME2 is '발신기관명2'
/

comment on column IF_KIS_ERP_006.BK_NAME3 is '발신기관 전자서명'
/

