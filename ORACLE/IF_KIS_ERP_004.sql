create table IF_KIS_ERP_004
(
	EDI_ID VARCHAR2(35) not null
		constraint PK_IF_KIS_ERP_004
			primary key,
	DATEE VARCHAR2(8) not null,
	EX_NAME1 VARCHAR2(54),
	EX_NAME2 VARCHAR2(54),
	DESC_TEXT CLOB,
	ETC_RFF_NO VARCHAR2(38),
	DOC VARCHAR2(105),
	BGM_CODE VARCHAR2(3),
	ERP_ID VARCHAR2(35)
)
/

comment on table IF_KIS_ERP_004 is '일반 응답서'
/

comment on column IF_KIS_ERP_004.EDI_ID is 'EDI 문서 신청번호'
/

comment on column IF_KIS_ERP_004.DATEE is '수신일자'
/

comment on column IF_KIS_ERP_004.EX_NAME1 is '발신자 상호'
/

comment on column IF_KIS_ERP_004.EX_NAME2 is '발신자 대표자'
/

comment on column IF_KIS_ERP_004.DESC_TEXT is '전달내용'
/

comment on column IF_KIS_ERP_004.ETC_RFF_NO is '기타참조번호'
/

comment on column IF_KIS_ERP_004.DOC is '관련서류'
/

comment on column IF_KIS_ERP_004.BGM_CODE is '구분코드'
/

comment on column IF_KIS_ERP_004.ERP_ID is 'ERP에서 전송한 키값'
/
